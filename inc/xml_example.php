<?php
$xmlstr = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<CstmrCdtTrfInitn>
		<GrpHdr>
			<MsgId>SEPATOOLNL20190724103534YSUTC</MsgId>
			<CreDtTm>2019-07-24T10:35:34</CreDtTm>
			<NbOfTxs>1</NbOfTxs>
			<CtrlSum>127.56</CtrlSum>
			<InitgPty>
				<Nm>Vouchercode BV</Nm>
			</InitgPty>
		</GrpHdr>
		<PmtInf>
			<PmtInfId>SEPATOOLNL201907241035341VUFSL</PmtInfId>
			<PmtMtd>TRF</PmtMtd>
			<BtchBookg>false</BtchBookg>
			<NbOfTxs>1</NbOfTxs>
			<CtrlSum>127.56</CtrlSum>
			<PmtTpInf>
				<SvcLvl>
					<Cd>NURG</Cd>
				</SvcLvl>
			</PmtTpInf>
			<ReqdExctnDt>2019-10-03</ReqdExctnDt>
			<Dbtr>
				<Nm>Vouchercode BV</Nm>
			</Dbtr>
			<DbtrAcct>
				<Id>
					<IBAN>NL14RABO0142984884</IBAN>
				</Id>
				<Ccy>USD</Ccy>
			</DbtrAcct>
			<DbtrAgt>
				<FinInstnId>
					<BIC>RABONL2U</BIC>
				</FinInstnId>
			</DbtrAgt>
			<CdtTrfTxInf>
				<PmtId>
					<EndToEndId>non ref</EndToEndId>
				</PmtId>
				<Amt>
					<InstdAmt Ccy="USD">127.56</InstdAmt>
				</Amt>
				<ChrgBr>SHAR</ChrgBr>
				<CdtrAgt>
					<FinInstnId>
						<BIC>WIREDEMM</BIC>
					</FinInstnId>
				</CdtrAgt>
				<Cdtr>
					<Nm>Nathemon Bhuranaphisitbhon</Nm>
					<PstlAdr>
						<StrtNm>Itsaraphap</StrtNm>
						<BldgNb>#224</BldgNb>
						<PstCd>10600</PstCd>
						<TwnNm>Bangkok-Yai</TwnNm>
						<CtrySubDvsn>Bangkok</CtrySubDvsn>
						<Ctry>TH</Ctry>
					</PstlAdr>
				</Cdtr>
				<CdtrAcct>
					<Id>
						<IBAN>DE37512308006504615963</IBAN>
					</Id>
				</CdtrAcct>
				<RmtInf>
					<Ustrd>VA and SEO TH | Invoice ID 008–2019</Ustrd>
				</RmtInf>
			</CdtTrfTxInf>
		</PmtInf>
	</CstmrCdtTrfInitn>
</Document>
XML;
