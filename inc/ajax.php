<?php

// AJAX file

// Get $xmlstr
include 'xml_example.php';

if (isset($_POST['our_company_name'])) {

	// Getting all parameters

	// Post parameters
	$our_company_name = isset($_POST['our_company_name']) ? $_POST['our_company_name'] : '';
	$our_iban = isset($_POST['our_iban']) ? $_POST['our_iban'] : '';
	$our_bic = isset($_POST['our_bic']) ? $_POST['our_bic'] : '';
	$payee_name = isset($_POST['payee_name']) ? $_POST['payee_name'] : '';
	$payee_address = isset($_POST['payee_address']) ? array_map('trim', explode(',', $_POST['payee_address'])) : '';
	$payee_iban = isset($_POST['payee_iban']) ? $_POST['payee_iban'] : '';
	$payee_bic = isset($_POST['payee_bic']) ? $_POST['payee_bic'] : '';
	$payee_iban_or_bban = isset($_POST['payee_iban_or_bban']) ? $_POST['payee_iban_or_bban'] : 'iban';
	$amount = isset($_POST['amount']) ? $_POST['amount'] : '';
	$description = isset($_POST['description']) ? $_POST['description'] : '';

	// Extra parameters
	$datetime = date("YmdHis");
	$MsgId = 'SEPATOOLNL'.$datetime.'YSUTC';
	$PmtInfId = 'SEPATOOLNL'.$datetime.'1VUFSL';
	$CreDtTm = date("Y-m-d\TH:i:s");
	$ReqdExctnDt = date("Y-m-d");


	// Set XML parameters
	$xmlobj = new SimpleXMLElement($xmlstr);

	$xmlobj->CstmrCdtTrfInitn->GrpHdr->MsgId = $MsgId;
	$xmlobj->CstmrCdtTrfInitn->GrpHdr->CreDtTm = $CreDtTm;
	$xmlobj->CstmrCdtTrfInitn->GrpHdr->CtrlSum = $amount;
	$xmlobj->CstmrCdtTrfInitn->GrpHdr->InitgPty->Nm = $our_company_name;

	$xmlobj->CstmrCdtTrfInitn->PmtInf->PmtInfId = $PmtInfId;
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CtrlSum = $amount;
	$xmlobj->CstmrCdtTrfInitn->PmtInf->ReqdExctnDt = $ReqdExctnDt;
	$xmlobj->CstmrCdtTrfInitn->PmtInf->Dbtr->Nm = $our_company_name;
	$xmlobj->CstmrCdtTrfInitn->PmtInf->DbtrAcct->Id->IBAN = $our_iban;
	$xmlobj->CstmrCdtTrfInitn->PmtInf->DbtrAgt->FinInstnId->BIC = $our_bic;

	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->Amt->InstdAmt = $amount;
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->CdtrAgt->FinInstnId->BIC = $payee_bic;
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->Cdtr->Nm = $payee_name;
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->Cdtr->PstlAdr->StrtNm = $payee_address[0];
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->Cdtr->PstlAdr->BldgNb = $payee_address[1];
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->Cdtr->PstlAdr->PstCd = $payee_address[2];
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->Cdtr->PstlAdr->TwnNm = $payee_address[3];
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->Cdtr->PstlAdr->CtrySubDvsn = $payee_address[4];
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->Cdtr->PstlAdr->Ctry = $payee_address[5];

	if ($payee_iban_or_bban == 'iban') {
		$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->CdtrAcct->Id->IBAN = $payee_iban;
	} else {
		unset($xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->CdtrAcct->Id->IBAN);
		$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->CdtrAcct->Id->Othr->Id = $payee_iban;
	}
	
	$xmlobj->CstmrCdtTrfInitn->PmtInf->CdtTrfTxInf->RmtInf->Ustrd = $description;

	// var_dump($payee_address);
	// var_dump($xmlobj);
	
	header('Content-disposition: attachment; filename="rabobank_payment.xml"');
	header('Content-type: "text/xml"; charset="utf8"');
	echo $xmlobj->asXML();
}