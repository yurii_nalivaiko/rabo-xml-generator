<!DOCTYPE html>
<html>
<head>
	<title>Rabobank XML Generator</title>
	<link rel="stylesheet" href="/rabo-xml-generator/assets/css/libs/bootstrap.min.css">
	<script src="/rabo-xml-generator/assets/js/libs/jquery-3.4.1.min.js"></script>
	<script src="/rabo-xml-generator/assets/js/libs/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="form-container d-block mx-auto mt-5 mb-5">
			<div class="col-md-12">
				<h1 class="mb-3">Rabobank XML Generator</h1>
			</div>
			<form id="xml_form" method="POST" action="/rabo-xml-generator/inc/ajax.php">
				<div class="form-group col-md-6 float-left">
					<label for="our_companies">Our companies</label>
					<select class="form-control" id="our_companies">
				      	<option value="rebel">REBEL Internet BV</option>
				      	<option value="vouchercode">Vouchercode BV</option>
				    </select>
				</div>
				<div class="form-group col-md-6 float-left">
					<label for="payee_bic">Payee BIC</label>
					<input type="text" class="form-control" id="payee_bic" name="payee_bic" required>
				</div>
				<div class="form-group col-md-6 float-left">
					<label for="our_company_name">Our company name</label>
					<input type="text" class="form-control" id="our_company_name" name="our_company_name" required>
				</div>
				<div class="form-group col-md-6 float-left">
					<label for="payee_name">Payee name (or beneficiary)</label>
					<input type="text" class="form-control" id="payee_name" name="payee_name" required>
				</div>
				<div class="form-group col-md-6 float-left">
					<label for="our_iban">Our IBAN</label>
					<input type="text" class="form-control" id="our_iban" name="our_iban" required>
				</div>
				<div class="form-group col-md-6 float-left">
					<label class="col-md-12" for="payee_iban_or_bban">Payee IBAN (or BBAN)</label>
					<input type="text" class="form-control col-md-8 float-left" id="payee_iban" name="payee_iban" required>
					<select class="form-control col-md-3 float-right" id="payee_iban_or_bban" name="payee_iban_or_bban">
				      	<option value="iban">IBAN</option>
				      	<option value="bban">BBAN</option>
				    </select>
				</div>
				<div class="form-group col-md-6 float-left">
					<label for="our_bic">Our BIC</label>
					<input type="text" class="form-control" id="our_bic" name="our_bic" required>
				</div>
				<div class="form-group col-md-6 float-left">
					<label for="amount">Amount to pay</label>
					<input type="text" class="form-control" id="amount" name="amount" required>
				</div>
				<div class="form-group col-md-6 float-left">
					<label for="description">Description (note)</label>
					<textarea id="description" name="description" class="form-control" rows="3"></textarea>
				</div>
				<div class="form-group col-md-6 float-left">
					<label for="payee_address">Payee address</label>
					<textarea id="payee_address" name="payee_address" class="form-control" rows="3" required></textarea>
					<small id="emailHelp" class="form-text text-muted">Please, add a comma-separated address (Street Name, Building Number, Post Code, Town Name, Country Sub Division, Country code). Example : Itsaraphap, #224, 10600, Bangkok-Yai, Bangkok, TH</small>
				</div>
				<div class="form-group col-md-12 overflow-hidden">
					<button type="submit" class="btn btn-primary float-left">Download XML</button>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		var companies = {
			'rebel' : {
				'our_company_name' : 'REBEL Internet BV',
				'our_iban' : 'NL09RABO0144571021',
				'our_bic' : 'RABONL2U'
			},
			'vouchercode' : {
				'our_company_name' : 'Vouchercode BV',
				'our_iban' : 'NL14RABO0142984884',
				'our_bic' : 'RABONL2U'
			},
		};
	</script>
	<script src="/rabo-xml-generator/assets/js/main.js"></script>
</body>
</html>