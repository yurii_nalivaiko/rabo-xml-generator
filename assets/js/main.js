// Main JS

function changeValues(item) {
	jQuery('#our_company_name').val(companies[item]['our_company_name']);
	jQuery('#our_iban').val(companies[item]['our_iban']);
	jQuery('#our_bic').val(companies[item]['our_bic']);
}

jQuery('#our_companies').on('change', function(){
	let item = jQuery(this).val();
	changeValues(item);
});

jQuery(window).on('load', function(){
	changeValues('rebel');
});